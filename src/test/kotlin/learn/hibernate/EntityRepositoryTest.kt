package learn.hibernate

import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class EntityRepositoryTest{

    @Autowired
    lateinit var messageRepository: MessageRepository

    @Test
    fun test(){
        Assert.assertTrue(messageRepository.findAll().isEmpty())
    }

}